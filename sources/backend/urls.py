from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from backend import settings
from core import views as core_views
from core.views import RegisterView, LoginView

urlpatterns = [  # Homepage
                  path('', core_views.index, name='home'),

                  # Auth route
                  path('login', LoginView.as_view(), name="login"),
                  path('register', RegisterView.as_view(), name="register"),
                  path('logout', core_views.logout, name="logout"),

                  # Dashboard
                  path('dashboard', core_views.index, name="dashboard"),
                  # Follows
                  path('follows', include('following.urls')),

                  # Tickets
                  path('tickets/', include('reviewing.urls')),

                  # Feed
                  path('feed', include('feed.urls')),

                  # Admin
                  path('admin/', admin.site.urls),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
