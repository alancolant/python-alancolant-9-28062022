/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["../**/*.{html,css}"],
    theme: {
        colors: {
            black: '#333333',
            primary: '#086552',
            secondary: '#00B295',
            grey: '#707070',
            'grey-medium': '#D3D3D3',
            'grey-light': '#EBEBEB',
            'grey-lightest': '#F8F8F8',
            white: '#ffffff',
            red: {
                600: '#dc2626',
                700: '#b91c1c'
            }
        },
        fontFamily: {
            roboto: 'Roboto, sans-serif',
            'roboto-slab': '"Roboto Slab",sans serif',
        },
        fontSize: {
            xxs: ['10px', '21px'],
            xs: ['12px', '14px'],
            sm: ['16px', '21px'],
            'sm-light': ['16px', '19px'],
            md: ['18px', '28px'],
            lg: ['20px', '28px'],
            xl: ['24px', '28px'],
            '2xl': ['28px', '37px'],
            '3xl-mobile': ['30px', '39.75px'],
            '3xl': ['40px', '53px'],
        },
        screens: {
            md: '768px',
            mdl: '992px',
            lg: '1120px',
            xl: '1360px',
            xxl: '1535px',
        },
    },
    plugins: [
        require('@tailwindcss/typography')
    ],
}
