from django.contrib.auth import login, logout as auth_logout
from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.views import View

from backend import settings
from core.forms.LoginForm import LoginForm
from core.forms.RegisterForm import RegisterForm


def index(request: HttpRequest):
    return redirect('/login')


class RegisterView(View):
    form = RegisterForm
    template_name = 'core/register.html'

    def get(self, request: HttpRequest):
        if request.user.is_authenticated:
            return redirect(settings.AUTH_LOGIN_REDIRECT)
        return render(request, self.template_name, context={"form": self.form})

    def post(self, request: HttpRequest):
        if request.user.is_authenticated:
            return redirect(settings.AUTH_LOGIN_REDIRECT)
        form = self.form(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(settings.AUTH_LOGIN_REDIRECT)
        else:
            return render(request, self.template_name, context={"form": form})


class LoginView(View):
    form = LoginForm
    template_name = 'core/login.html'

    def get(self, request: HttpRequest):
        if request.user.is_authenticated:
            return redirect(settings.AUTH_LOGIN_REDIRECT)
        return render(request, self.template_name, context={"form": self.form})

    def post(self, request: HttpRequest):
        if request.user.is_authenticated:
            return redirect(settings.AUTH_LOGIN_REDIRECT)
        form = self.form(request, request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect(settings.AUTH_LOGIN_REDIRECT)
        else:
            return render(request, self.template_name, context={"form": form})


def logout(request: HttpRequest):
    auth_logout(request)
    return redirect('home')
