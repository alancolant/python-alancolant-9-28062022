from itertools import chain

from django.db.models import Value
from django.http import HttpRequest, HttpResponseForbidden
from django.shortcuts import render, redirect
from django.views import View

from reviewing.forms.CreateReviewForm import CreateReviewForm
from reviewing.forms.CreateTicketForm import CreateTicketForm
from reviewing.models import Ticket, Review


# -------------------------------------------------------TICKETS--------------------------------------------------------
class TicketsView(View):
    template_name = 'reviewing/tickets/index.html'

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        tickets = Ticket.objects \
            .filter(user_id=request.user.id) \
            .prefetch_related('reviews') \
            .annotate(type=Value('Ticket')) \
            .order_by('-time_created')
        reviews = Review.objects \
            .filter(user_id=request.user.id) \
            .prefetch_related('ticket') \
            .annotate(type=Value('Review')) \
            .order_by('-time_created')

        self.posts = sorted(
            chain(reviews, tickets),
            key=lambda p: p.time_created,
            reverse=True
        )

    def get(self, request: HttpRequest):
        return render(request, self.template_name, context={'posts': self.posts})


class TicketCreateView(View):
    form = CreateTicketForm
    template_name = 'reviewing/tickets/create.html'

    def get(self, request: HttpRequest):
        return render(request, self.template_name, context={'form': self.form})

    def post(self, request: HttpRequest):
        form = self.form(request.POST, request.FILES, instance=Ticket(user=request.user))
        if form.is_valid():
            form.save()
            return redirect('feed.index')
        return render(request, self.template_name, context={'form': form})


class TicketEditView(View):
    template_name = 'reviewing/tickets/edit.html'
    form_class = CreateTicketForm

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.ticket = Ticket.objects.get(id=kwargs['ticket_id'])
        self.form = self.form_class(instance=self.ticket)

    def get(self, request: HttpRequest, ticket_id: int):
        if not self.ticket.can('edit', request.user):
            return HttpResponseForbidden()

        return render(request, self.template_name, context={'form': self.form, 'ticket': self.ticket})

    def post(self, request: HttpRequest, ticket_id: int):
        if not self.ticket.can('edit', request.user):
            return HttpResponseForbidden()

        form = self.form_class(request.POST, request.FILES, instance=self.ticket)
        if form.is_valid():
            form.save()
            return redirect('feed.index')
        return render(request, self.template_name, context={'form': form, 'ticket': self.ticket})


class TicketView(View):
    def dispatch(self, *args, **kwargs):
        method = self.request.GET.get('_method', '') or self.request.POST.get('_method', '')
        if method.lower() == 'delete':
            return self.delete(*args, **kwargs)
        return super().dispatch(*args, **kwargs)

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.ticket = Ticket.objects.get(id=kwargs['ticket_id'])

    def delete(self, request: HttpRequest, **kwargs):
        if not self.ticket.can('delete', request.user):
            return HttpResponseForbidden()
        self.ticket.delete()
        return redirect('reviewing.tickets')


# -------------------------------------------------------REVIEWS--------------------------------------------------------

class ReviewCreateView(View):
    form_class = CreateReviewForm
    template_name = 'reviewing/reviews/create.html'

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.ticket = Ticket.objects.get(id=kwargs['ticket_id'])
        self.form = self.form_class(instance=self.ticket)

    def get(self, request: HttpRequest, ticket_id: int):
        return render(request, self.template_name, context={'form': self.form, 'ticket': self.ticket})

    def post(self, request: HttpRequest, ticket_id: int):
        form = self.form_class(request.POST, instance=Review(ticket_id=self.ticket.id, user_id=request.user.id))
        if form.is_valid():
            form.save()
            return redirect('feed.index')
        return render(request, self.template_name, context={'form': form, 'ticket': self.ticket})


class ReviewEditView(View):
    template_name = 'reviewing/reviews/edit.html'
    form_class = CreateReviewForm

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.ticket = Ticket.objects.get(id=kwargs['ticket_id'])
        self.review = Review.objects.get(id=kwargs['review_id'])
        self.form = self.form_class(instance=self.review)

    def get(self, request: HttpRequest, **kwargs):
        if not self.review.can('edit', request.user):
            return HttpResponseForbidden()

        return render(request,
                      self.template_name,
                      context={'form': self.form, 'ticket': self.ticket, 'review': self.review})

    def post(self, request: HttpRequest, **kwargs):
        if not self.review.can('edit', request.user):
            return HttpResponseForbidden()

        form = self.form_class(request.POST, instance=self.review)
        if form.is_valid():
            form.save()
            return redirect('feed.index')
        return render(request, self.template_name, context={'form': form, 'ticket': self.ticket, 'review': self.review})


class ReviewView(View):
    def dispatch(self, *args, **kwargs):
        method = self.request.GET.get('_method', '') or self.request.POST.get('_method', '')
        if method.lower() == 'delete':
            return self.delete(*args, **kwargs)
        return super().dispatch(*args, **kwargs)

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.review = Review.objects.get(id=kwargs['review_id'])

    def delete(self, request: HttpRequest, **kwargs):
        if not self.review.can('delete', request.user):
            return HttpResponseForbidden()
        self.review.delete()
        return redirect('feed.index')


# ------------------------------------------------COMBINED---------------------------------------------------------------

class ReviewTicketCreateView(View):
    ticket_form_class = CreateTicketForm
    review_form_class = CreateReviewForm
    template_name = 'reviewing/combined/create.html'

    def get(self, request: HttpRequest):
        return render(request, self.template_name, context={
            'ticket_form': self.ticket_form_class,
            'review_form': self.review_form_class
        })

    def post(self, request: HttpRequest):
        ticket_form = self.ticket_form_class(request.POST, request.FILES, instance=Ticket(user_id=request.user.id))
        review_form = self.review_form_class(request.POST, instance=Review(user_id=request.user.id))

        ticket_valid = ticket_form.is_valid()
        review_valid = review_form.is_valid()

        if ticket_valid and review_valid:
            ticket = ticket_form.save()
            review_form.instance.ticket_id = ticket.id
            review_form.save()
            return redirect('feed.index')

        return render(request, self.template_name, context={
            'ticket_form': ticket_form,
            'review_form': review_form
        })
