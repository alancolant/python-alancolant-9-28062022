from django.urls import path

from .views import TicketsView, TicketView, ReviewView, TicketCreateView, TicketEditView, ReviewCreateView, \
    ReviewEditView, ReviewTicketCreateView

urlpatterns = [
    # Tickets
    path('', TicketsView.as_view(), name="reviewing.tickets"),
    path('create', TicketCreateView.as_view(), name="reviewing.tickets.create"),

    # Ticket
    path('<int:ticket_id>', TicketView.as_view(), name="reviewing.ticket"),
    path('<int:ticket_id>/edit', TicketEditView.as_view(), name="reviewing.ticket.edit"),

    # Reviews
    path('<int:ticket_id>/reviews/create', ReviewCreateView.as_view(), name="reviewing.reviews.create"),

    # Review
    path('<int:ticket_id>/reviews/<int:review_id>', ReviewView.as_view(), name="reviewing.review"),
    path('<int:ticket_id>/reviews/<int:review_id>/edit', ReviewEditView.as_view(), name="reviewing.review.edit"),

    # Combined
    path('reviews/create', ReviewTicketCreateView.as_view(), name="reviewing.combined.create")

]
