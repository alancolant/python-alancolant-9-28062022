from django.forms import ModelForm

from reviewing.models import Ticket


class CreateTicketForm(ModelForm):
    class Meta:
        model = Ticket
        exclude = ['user']
