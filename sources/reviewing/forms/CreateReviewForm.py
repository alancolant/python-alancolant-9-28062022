from django.forms import ModelForm

from reviewing.models import Review


class CreateReviewForm(ModelForm):
    class Meta:
        model = Review
        exclude = ['user', 'ticket']
