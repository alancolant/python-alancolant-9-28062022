from django.apps import AppConfig


class ReviewingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'reviewing'
