from django import forms


class FollowUserForm(forms.Form):
    followed_user = forms.ChoiceField(
        label='Followed user',
    )

    def __init__(self, *args, choices):
        super().__init__(*args)
        self.fields['followed_user'].choices = choices
