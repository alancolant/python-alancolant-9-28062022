from django.urls import path

from .views import index, unfollow

urlpatterns = [
    path('', index, name="following.index"),
    path('<str:username>', unfollow, name="following.unfollow"),
]
