from django.db.models import Exists, OuterRef
from django.http import HttpRequest
from django.shortcuts import render, redirect

from core.models import User
from following.forms.FollowUserForm import FollowUserForm
from following.models import UserFollows


def index(request: HttpRequest):
    followings = User.objects \
        .exclude(id=request.user.id) \
        .filter(Exists(UserFollows.objects.filter(user=request.user, followed_user_id=OuterRef('id'))))

    followers = User.objects \
        .exclude(id=request.user.id) \
        .filter(Exists(UserFollows.objects.filter(user_id=OuterRef('id'), followed_user=request.user)))

    followables = User.objects.exclude(id=request.user.id) \
        .filter(~Exists(UserFollows.objects.filter(user=request.user, followed_user_id=OuterRef('id'))))

    form = FollowUserForm(choices=[(u.username, u.username) for u in followables])

    if request.method == "POST":
        form = FollowUserForm(request.POST, choices=[(u.username, u.username) for u in followables])
        if form.is_valid():
            follow = UserFollows()
            follow.followed_user = User.objects.get(username=form.cleaned_data.get('followed_user'))
            follow.user = request.user
            follow.save()
            return redirect("following.index")

    return render(request, "following/index.html",
                  context={'form': form, 'followings': followings, 'followers': followers})


def unfollow(request: HttpRequest, username: str):
    follow = UserFollows.objects.filter(user=request.user, followed_user=User.objects.get(username=username))
    follow.delete()
    return redirect("following.index")
