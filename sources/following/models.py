from django.conf import settings
from django.db import models
from django.db.models import Manager


class UserFollows(models.Model):
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="following")
    followed_user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="followed_by")

    objects = Manager()
    
    class Meta:
        unique_together = ('user', 'followed_user')
