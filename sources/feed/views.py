from itertools import chain

from django.db.models import Value, Q, Exists, OuterRef
from django.http import HttpRequest
from django.shortcuts import render
from django.views import View

from following.models import UserFollows
from reviewing.models import Ticket, Review


# Create your views here.
class FeedView(View):
    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        tickets = Ticket.objects.filter(
            Q(user_id=request.user.id)
            | Exists(UserFollows.objects.filter(
                user_id=request.user.id,
                followed_user_id=OuterRef('user_id')
            ))
        ).annotate(type=Value('Ticket')) \
            .prefetch_related('reviews') \
            .order_by('-time_created')

        reviews = Review.objects.filter(
            Q(user_id=request.user.id)
            | Exists(UserFollows.objects.filter(
                user_id=request.user.id,
                followed_user_id=OuterRef('user_id')
            ))
        ).annotate(type=Value('Review')) \
            .prefetch_related('ticket') \
            .order_by('-time_created')

        self.posts = sorted(
            chain(reviews, tickets),
            key=lambda p: p.time_created,
            reverse=True
        )

    def get(self, request: HttpRequest):
        return render(request, 'feed/index.html', context={'posts': self.posts})
# Exists(
# User.objects.filter(
#     Exists(UserFollows.objects.filter(user=request.user, followed_user_id=OuterRef(OuterRef('user_id'))))
# ))
