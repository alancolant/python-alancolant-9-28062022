# Requirements

- Python 3.9.x
- Node `For dev usage only`

# Usage

## With venv

```sh
python3 -m venv /path/to/new/virtual/environment #Create venv
source /path/to/new/virtual/environment/bin/activate 

cd sources
pip install -r requirements.txt

python manage.py runserver
```

## Without venv

```sh
cd sources
pip install -r requirements.txt
python manage.py runserver
```

# Development

## Tailwind

```bash
cd sources/core/
yarn # ou npm install
yarn dev # ou npm run watch > For watch
yarn build # ou npm run build > For prod
```

# Connexion

## Interface

* http://127.0.0.1:8000/
* User: `demo1` *(demo1 - demo2 - ...)*
* Password: `@password1` *(@password1 - @password2 - ...)*